from pathlib import Path

import pandas
import pytest

from ml_wrapper.models.sklearn_lr_model import LendingClubSKLearnLR
from ml_wrapper.validators.data_schema import Constants
from ml_wrapper.validators.sklearn_logistic_regression_schema import HyperParam, ModelParam, OptunaCVParam, JobParam

PKG_PATH = Path(__file__).parents[1].resolve()
TEST_MODELS_DIR = PKG_PATH / "tests/models"
TEST_MODEL_FILE = TEST_MODELS_DIR / "test_logistic_regression.joblib"


@pytest.fixture
def test_data_path():
    return Constants().data_file


@pytest.fixture
def test_df(test_data_path):
    return pandas.read_csv(test_data_path)


@pytest.fixture
def test_model_file():
    return TEST_MODEL_FILE


@pytest.fixture
def test_model(test_model_file):
    hyper_parameters = HyperParam()
    model_parameters = ModelParam()
    job_parameters = JobParam(model_file=test_model_file)
    optuna_cv_parameters = OptunaCVParam()
    return LendingClubSKLearnLR(hyper_parameters, model_parameters, job_parameters, optuna_cv_parameters)
