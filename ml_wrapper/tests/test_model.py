import os

import hypothesis
import hypothesis.strategies as strategies
import pytest
from pandera import SchemaModel
from pydantic import BaseModel, BaseConfig
from sklearn import metrics
from scipy import stats
from scipy.spatial import distance

from ml_wrapper.validators.data_schema import InputDataFrameSchema, PreprocessedDataSchema, Constants
from ml_wrapper.validators.sklearn_logistic_regression_schema import (
    JobParam,
    HyperParam,
    HyperC,
    HyperL1Ratio,
    HyperMaxIter,
    ModelParam,
    OptunaCVParam,
)


def test_if_datamodels_are_valid():
    assert issubclass(InputDataFrameSchema, SchemaModel)
    assert issubclass(PreprocessedDataSchema, SchemaModel)
    assert issubclass(HyperParam, BaseModel)
    assert issubclass(HyperC, BaseModel)
    assert issubclass(HyperMaxIter, BaseModel)
    assert issubclass(HyperL1Ratio, BaseModel)
    assert issubclass(JobParam, BaseModel)
    assert issubclass(OptunaCVParam, BaseModel)
    assert issubclass(ModelParam, BaseModel)
    assert issubclass(HyperParam.__config__, BaseConfig)
    assert issubclass(HyperC.__config__, BaseConfig)
    assert issubclass(HyperMaxIter.__config__, BaseConfig)
    assert issubclass(HyperL1Ratio.__config__, BaseConfig)
    assert issubclass(JobParam.__config__, BaseConfig)
    assert issubclass(Constants, BaseModel)
    assert issubclass(Constants.__config__, BaseConfig)
    assert issubclass(OptunaCVParam.__config__, BaseConfig)
    assert issubclass(ModelParam.__config__, BaseConfig)


@hypothesis.given(strategies.builds(JobParam))
def test_job_parameter_datamodel(data):
    assert isinstance(data, JobParam)


@hypothesis.given(
    strategies.builds(
        HyperParam,
        C=strategies.builds(HyperC, value=strategies.floats(min_value=HyperC().min, max_value=HyperC().max)),
        max_iter=strategies.builds(
            HyperMaxIter, value=strategies.integers(min_value=HyperMaxIter().min, max_value=HyperMaxIter().max)
        ),
        l1_ratio=strategies.builds(
            HyperL1Ratio, value=strategies.floats(min_value=HyperL1Ratio().min, max_value=HyperL1Ratio().max)
        ),
    )
)
def test_hyper_parameter_datamodel(data):
    assert isinstance(data, HyperParam)


@hypothesis.given(strategies.builds(ModelParam))
def test_solver_parameters_datamodel(data):
    assert isinstance(data, ModelParam)


@hypothesis.given(strategies.builds(OptunaCVParam))
def test_optuna_cv_parameters_datamodel(data):
    assert isinstance(data, OptunaCVParam)


def test_logistic_model_fit(test_df, test_model):
    target = Constants().target
    x, y = test_df.drop(target, axis=1), test_df[target].values
    if os.path.exists(test_model.job_parameters.model_file):
        os.remove(test_model.job_parameters.model_file)
    test_model.fit(x, y)
    assert os.path.exists(test_model.job_parameters.model_file)
    os.remove(test_model.job_parameters.model_file)


def test_logistic_model_tune_parameters(test_df, test_model):
    target = Constants().target
    x, y = test_df.drop(target, axis=1), test_df[target].values
    if os.path.exists(test_model.job_parameters.model_file):
        os.remove(test_model.job_parameters.model_file)
    _ = test_model.tune_parameters(x, y)
    assert os.path.exists(test_model.job_parameters.model_file)


def test_logistic_model_predict(test_df, test_model):
    target = Constants().target
    x, y = test_df.drop(target, axis=1), test_df[target].values
    preds = test_model.predict(x)
    assert stats.ttest_ind(y, preds)[1] <= 0.05


def test_logistic_model_predict_proba(test_df, test_model):
    target = Constants().target
    x, y = test_df.drop(target, axis=1), test_df[target].values
    preds = test_model.predict_proba(x)
    preds = preds[:, 1]
    t_test = stats.ttest_ind_from_stats(
        mean1=y.mean(), mean2=preds.mean(), std1=y.std(), std2=preds.std(), nobs1=y.shape[0], nobs2=preds.shape[0]
    )
    p_val = t_test[1]
    assert p_val <= 0.05


def test_logistic_model_evaluate(test_df, test_model):
    target = Constants().target
    x, y = test_df.drop(target, axis=1), test_df[target].values
    results = test_model.evaluate(x, y)
    assert results["auc"] > 0.6
    os.remove(test_model.job_parameters.model_file)
