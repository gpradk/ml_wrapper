from abc import ABC, abstractmethod


class LendingClubModelError(Exception):
    pass


class BaseWrapperModel(ABC):
    @abstractmethod
    def fit(self, x, y):
        pass

    @abstractmethod
    def predict(self, x):
        pass

    @abstractmethod
    def predict_proba(self, x):
        pass

    @abstractmethod
    def evaluate(self, x, y):
        pass

    @abstractmethod
    def tune_parameters(self, x, y):
        pass
