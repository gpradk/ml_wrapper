import os

import joblib
import numpy
import pandas
import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from sklearn import metrics
from sklearn.model_selection import train_test_split, RepeatedStratifiedKFold, StratifiedKFold
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader

from ml_wrapper.validators.data_schema import Constants, InputDataFrameSchema, PreprocessedDataSchema
from ml_wrapper.validators.lightning_classification_schema import (
    TrainerSchema,
    CheckPointSchema,
    EarlyStoppingSchema,
    DataLoaderSchema,
)
from ml_wrapper.modules.crossvalidation import CrossValidationDataModule
from ml_wrapper.modules.dataset import LendingClubDataset
from ml_wrapper.modules.nn_modules import BaselineClassifier
from ml_wrapper.models.basemodel import BaseWrapperModel
from ml_wrapper.utils import (
    drop_columns,
    preprocess_emp_length,
    preprocess_categorical_columns,
    preprocess_purpose_cat,
    fillnans,
    train_minmax,
    train_oh_encode,
    make_categorical_columns,
)


class LightningWrapperModel(BaseWrapperModel):
    def __init__(self, hyper_parameters, model_parameters, job_parameters, cv_parameters):
        self.hyper_parameters = hyper_parameters
        self.model_parameters = model_parameters
        self.job_parameters = job_parameters
        self.cv_parameters = cv_parameters
        self.constants = Constants()
        if self.cv_parameters.n_repeats >= 1:
            self.kf = RepeatedStratifiedKFold(
                n_splits=cv_parameters.n_splits,
                n_repeats=cv_parameters.n_repeats,
                random_state=job_parameters.random_state,
            )
        else:
            self.kf = StratifiedKFold(
                n_splits=cv_parameters.n_splits, random_state=job_parameters.random_state, shuffle=True
            )
        self.train_dataloader = None
        self.val_dataloader = None
        self.test_dataloader = None

    def preprocess(self, x):
        """
        Preprocessing function.

        :param x:pandas.DataFrame
        :return:pandas.DataFrame
        """

        valid_input_df = InputDataFrameSchema.validate(x)
        preprocessed_df = drop_columns(valid_input_df, self.constants.drop)
        preprocessed_df = preprocess_emp_length(preprocessed_df)
        preprocessed_df = preprocess_categorical_columns(preprocessed_df, self.constants.one_hot)
        preprocessed_df = preprocess_purpose_cat(preprocessed_df)

        for col in preprocessed_df.columns:
            preprocessed_df = fillnans(preprocessed_df, col)
        assert not pandas.isnull(preprocessed_df).values.sum() > 0
        return preprocessed_df

    @staticmethod
    def preprocess_target(y: numpy.ndarray) -> numpy.ndarray:
        """
        Preprocessing function for targets.

        """
        label_enc = LabelEncoder()
        return label_enc.fit_transform(y)

    def train_encode(self, x):
        """
        Train encoders for validators.
        This function trains a minmax encoder and an one hot encoder.
        It validates the encoded output with the preprocessed schema model.

         :param x: pandas.DataFrame
        :return:  pandas.DataFrame
        """
        x, minmax = train_minmax(x, self.constants.minmax)
        x, oh = train_oh_encode(x, self.constants.one_hot)
        valid_df = PreprocessedDataSchema.validate(x)
        assert not pandas.isnull(valid_df).values.sum() > 0
        joblib.dump(minmax, self.constants.minmax_encoder_filename)
        joblib.dump(oh, self.constants.onehot_encoder_filename)
        return valid_df

    def predict_encode(self, x):
        """
        Encode validators for predictions.
        This function fetches trained and saved encoders and uses them to encode
        validators for model predictions.
        The output is validated using the preprocessed schema model.

        :param x: pandas.DataFrame
        :return: pandas.DataFrame
        """
        minmax = joblib.load(self.constants.minmax_encoder_filename)
        oh = joblib.load(self.constants.onehot_encoder_filename)

        x[self.constants.minmax] = minmax.transform(x[self.constants.minmax])
        x_one_hot = oh.transform(x[self.constants.one_hot])
        x_one_hot = pandas.DataFrame(x_one_hot.todense())
        x_one_hot = make_categorical_columns(x_one_hot, categories=oh.categories_, columns=self.constants.one_hot)
        x = x.drop(columns=self.constants.one_hot)
        x = pandas.concat([x, x_one_hot], axis=1)
        return PreprocessedDataSchema.validate(x)

    def fit(self, x, y):
        x = self.preprocess(x)
        x = self.train_encode(x)
        x = torch.tensor(x.values)
        y_target = self.preprocess_target(y)
        y_target = torch.tensor(y_target)

        # model = MLPModel(**self.model_parameters.dict())
        model = BaselineClassifier(**self.model_parameters.dict())

        if (self.train_dataloader is None) and (self.val_dataloader is None) and (self.test_dataloader is None):
            x_cv, x_holdout, y_cv, y_holdout = train_test_split(
                x, y_target, random_state=self.job_parameters.random_state, stratify=y_target
            )
            training_dataset = LendingClubDataset(x_cv, y_cv)
            validation_dataset = LendingClubDataset(x_holdout, y_holdout)

            self.train_dataloader = DataLoader(**DataLoaderSchema(dataset=training_dataset).dict())
            self.val_dataloader = DataLoader(**DataLoaderSchema(dataset=validation_dataset, shuffle=False).dict())

        model_checkpoint = ModelCheckpoint(**CheckPointSchema().dict())
        early_stop_callback = EarlyStopping(**EarlyStoppingSchema().dict())
        trainer_schema = TrainerSchema(auto_lr_find=True, callbacks=[early_stop_callback, model_checkpoint])
        trainer = pl.Trainer(**trainer_schema.dict())
        trainer.fit(model, train_dataloader=self.train_dataloader, val_dataloaders=self.val_dataloader)

    def tune_parameters(self, x, y):
        x = self.preprocess(x)
        x = self.train_encode(x)
        x = torch.tensor(x.values)
        y_target = self.preprocess_target(y)
        y_target = torch.tensor(y_target)
        x_cv, x_holdout, y_cv, y_holdout = train_test_split(
            x, y_target, random_state=self.job_parameters.random_state, stratify=y_target
        )
        loaders = CrossValidationDataModule(
            x_cv,
            y_cv,
            kf=self.kf,
            random_state=self.job_parameters.random_state,
            batch_size=self.job_parameters.batch_size,
            num_workers=self.job_parameters.cpus,
            hold_out_split=self.cv_parameters.hold_out_split,
            shuffle=self.cv_parameters.shuffle,
        )
        cv_performance = []
        cv_models = []
        cv_predictions = []
        cv_scores = []
        x_holdout = torch.tensor(x_holdout).type(torch.float)
        for cv_split, cv in enumerate(loaders.cv_dataloaders()):
            # model = MLPModel(**self.model_parameters.dict())
            model = BaselineClassifier(**self.model_parameters.dict())
            # model_checkpoint = ModelCheckpoint(**CheckPointSchema().dict())
            early_stop_callback = EarlyStopping(**EarlyStoppingSchema().dict())
            trainer_schema = TrainerSchema(auto_lr_find=True, callbacks=[early_stop_callback])
            trainer = pl.Trainer(**trainer_schema.dict())
            trainer.fit(model, train_dataloader=cv["train_dataloader"], val_dataloaders=cv["val_dataloader"])
            cv_performance.append(trainer.test(model, test_dataloaders=cv["test_dataloader"]))
            model.to("cpu")
            test_predictions = model(x_holdout)
            test_predictions = test_predictions.detach().to("cpu").numpy()
            cv_models.append(model)
            cv_predictions.append(test_predictions)
            fpr, tpr, thresholds = metrics.roc_curve(y_true=y_holdout, y_score=test_predictions[:, 1], pos_label=1)
            auc = metrics.auc(fpr, tpr)
            cv_scores.append(auc)

        best_model = cv_models[numpy.argmax(cv_scores)]
        best_ckpt = CheckPointSchema(filename=f"best_model.pth")
        torch.save(best_model.state_dict(), os.path.join(best_ckpt.dirpath, best_ckpt.filename))
        x_eval = x.clone().detach().type(torch.float32)
        output = best_model(x_eval)
        output = output.detach().numpy()
        fpr, tpr, thresholds = metrics.roc_curve(y_true=y_target, y_score=output[:, 1], pos_label=1)
        return fpr, tpr, thresholds, output, y_target.detach().numpy().astype(float)

    def evaluate(self, x, y):
        pass

    def predict(self, x):
        pass

    def predict_proba(self, x):
        pass
