from argparse import ArgumentParser

import pytorch_lightning as pl
import torch
import torch.nn.functional as F

from mlp import MLP


class BaselineClassifier(pl.LightningModule):
    def __init__(
        self,
        input_dim: int,
        optimizer: str,
        num_classes: int = 2,
        bias: bool = True,
        learning_rate: float = 1e-4,
        l1_strength: float = 0.0,
        l2_strength: float = 0.0,
        dropout: float = 0.0,
        **kwargs,
    ):
        """

        Baseline Logistic regression using PyTorch and Pytorch-Lightning.

        Reference: Pytorch-Lightning repo

         Args:
            input_dim: number of dimensions of the input (at least 1)
            num_classes: number of class labels (binary: 2, multi-class: >2)
            bias: specifies if a constant or intercept should be fitted (equivalent to fit_intercept in sklearn)
            learning_rate: learning_rate for the optimizer
            optimizer: the optimizer to use (default='Adam')
            l1_strength: L1 regularization strength (default=None)
            l2_strength: L2 regularization strength (default=None)"""
        super().__init__()

        # Schema validation

        # self.trainer_params = TrainerSchema().dict()
        # self.hparams = {**self.model_hparams, **self.trainer_params}

        self.l2_strength = l2_strength
        self.l1_strength = l1_strength
        self.learning_rate = learning_rate
        self.input_dim = input_dim
        self.num_classes = num_classes
        self.dropout = dropout
        self.bias = bias
        self.save_hyperparameters()

        self.linear_out = torch.nn.Linear(self.input_dim, self.num_classes)
        self.optimizer = getattr(torch.optim, optimizer)
        self.linear_out.apply(self.init_weights)

        self.train_f1 = pl.metrics.F1(num_classes=num_classes)
        self.val_f1 = pl.metrics.F1(num_classes=num_classes)
        self.test_f1 = pl.metrics.F1(num_classes=num_classes)

    def forward(self, x):
        x = self.linear_out(x)
        return F.softmax(x, dim=-1)

    @staticmethod
    def init_weights(m):
        if type(m) == torch.nn.Linear:
            torch.nn.init.xavier_uniform_(m.weight)

    def training_step(self, batch, batch_idx):
        x, y = batch
        x.view(x.size(0), -1)
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, target=y, reduction="sum")

        if self.hparams.l1_strength > 0:
            l1_reg = sum(param.abs().sum() for param in self.parameters())
            loss += self.hparams.l1_strength * l1_reg

        if self.hparams.l2_strength > 0:
            l2_reg = sum(param.pow(2).sum() for param in self.parameters())
            loss += self.hparams.l2_strength * l2_reg

        loss /= x.size(0)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("train_f1", self.train_f1(y_hat, y))
        return {"loss": loss}

    def training_epoch_end(self, outputs):
        self.log("train_f1_epoch", self.train_f1.compute(), on_epoch=True, prog_bar=True)

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, target=y, reduction="sum")
        loss /= x.size(0)
        self.log("val_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("val_f1", self.val_f1(y_hat, y))
        return {"val_loss": loss}

    def validation_epoch_end(self, outputs):
        val_loss = torch.stack([x["val_loss"] for x in outputs]).mean()
        self.log("val_loss_epoch", val_loss, on_epoch=True, prog_bar=False)
        self.log("val_f1_epoch", self.val_f1.compute(), on_epoch=True, prog_bar=True)

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, target=y, reduction="sum")
        loss /= x.size(0)
        self.log("test_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("test_f1", self.test_f1(y_hat, y))
        return {"test_loss": loss}

    def test_epoch_end(self, outputs):
        test_loss = torch.stack([x["test_loss"] for x in outputs]).mean()
        self.log("test_loss_epoch", test_loss, on_epoch=True, prog_bar=False)
        self.log("test_f1_epoch", self.test_f1.compute(), on_epoch=True, prog_bar=True)

    def configure_optimizers(self):
        return self.optimizer(self.parameters(), lr=self.hparams.learning_rate)

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument("--learning_rate", type=float, default=0.0001)
        parser.add_argument("--input_dim", type=int, default=None)
        parser.add_argument("--output_dim", type=int, default=1)
        parser.add_argument("--bias", default="store_true")
        parser.add_argument("--batch_size", type=int, default=16)
        return parser


class MLPModel(pl.LightningModule):
    def __init__(
        self,
        input_dim: int,
        hidden_dim: int,
        n_hidden: int,
        optimizer: str,
        output_dim: int = 2,
        num_classes: int = 2,
        bias: bool = True,
        learning_rate: float = 1e-4,
        l1_strength: float = 0.0,
        l2_strength: float = 0.0,
        dropout: float = 0.0,
        **kwargs,
    ):
        """
        Args:
           input_dim: number of dimensions of the input (at least 1)
           num_classes: number of class labels (binary: 2, multi-class: >2)
           bias: specifies if a constant or intercept should be fitted (equivalent to fit_intercept in sklearn)
           learning_rate: learning_rate for the optimizer
           optimizer: the optimizer to use (default='Adam')
           l1_strength: L1 regularization strength (default=None)
           l2_strength: L2 regularization strength (default=None)"""
        super().__init__()

        self.l2_strength = l2_strength
        self.l1_strength = l1_strength
        self.learning_rate = learning_rate
        self.output_dim = output_dim
        self.input_dim = input_dim
        self.num_classes = num_classes
        self.hidden_dim = hidden_dim
        self.n_hidden = n_hidden
        self.dropout = dropout
        self.bias = bias
        self.save_hyperparameters()

        self.mlp = MLP(
            input_dim=input_dim,
            num_classes=output_dim,
            hidden_dim=hidden_dim,
            n_hidden=n_hidden,
            bias=bias,
            dropout=dropout,
        )
        self.linear_out = torch.nn.Linear(self.output_dim, self.num_classes)
        self.optimizer = getattr(torch.optim, optimizer)
        self.linear_out.apply(self.init_weights)

        self.train_f1 = pl.metrics.F1(num_classes=num_classes, threshold=0.5)
        self.val_f1 = pl.metrics.F1(num_classes=num_classes, threshold=0.5)
        self.test_f1 = pl.metrics.F1(num_classes=num_classes, threshold=0.5)

    def forward(self, x):
        x = self.mlp(x)
        x = self.linear_out(x)
        return F.softmax(x, dim=-1)

    @staticmethod
    def init_weights(m):
        if type(m) == torch.nn.Linear:
            torch.nn.init.xavier_uniform_(m.weight)

    def training_step(self, batch, batch_idx):
        x, y = batch

        # flatten any input
        x.view(x.size(0), -1)
        y_hat = self(x)
        loss = torch.nn.functional.cross_entropy(y_hat.squeeze(), y.squeeze(), reduction="sum")

        # L1 regularizer
        if self.hparams.l1_strength > 0:
            l1_reg = sum(param.abs().sum() for param in self.parameters())
            loss += self.hparams.l1_strength * l1_reg

        # L2 regularizer
        if self.hparams.l2_strength > 0:
            l2_reg = sum(param.pow(2).sum() for param in self.parameters())
            loss += self.hparams.l2_strength * l2_reg

        loss /= x.size(0)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("train_f1", self.train_f1(y_hat, y))
        return {"loss": loss}

    def training_epoch_end(self, outputs):
        self.log("train_f1_epoch", self.train_f1.compute(), on_epoch=True, prog_bar=True)

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, target=y, reduction="sum")
        loss /= x.size(0)
        self.log("val_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("val_f1", self.val_f1(y_hat, y))
        return {"val_loss": loss}

    def validation_epoch_end(self, outputs):
        val_loss = torch.stack([x["val_loss"] for x in outputs]).mean()
        self.log("val_loss_epoch", val_loss, on_epoch=True, prog_bar=False)
        self.log("val_f1_epoch", self.val_f1.compute(), on_epoch=True, prog_bar=True)

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, target=y, reduction="sum")
        loss /= x.size(0)
        self.log("test_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        self.log("test_f1", self.test_f1(y_hat, y))
        return {"test_loss": loss}

    def test_epoch_end(self, outputs):
        test_loss = torch.stack([x["test_loss"] for x in outputs]).mean()
        self.log("test_loss_epoch", test_loss, on_epoch=True, prog_bar=False)
        self.log("test_f1_epoch", self.test_f1.compute(), on_epoch=True, prog_bar=True)

    def configure_optimizers(self):
        return self.optimizer(self.parameters(), lr=self.hparams.learning_rate)

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument("--learning_rate", type=float, default=0.0001)
        parser.add_argument("--input_dim", type=int, default=None)
        parser.add_argument("--output_dim", type=int, default=1)
        parser.add_argument("--bias", default="store_true")
        parser.add_argument("--batch_size", type=int, default=16)
        return parser
