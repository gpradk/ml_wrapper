from typing import Any

import pandas
import torch
from torch.utils.data import Dataset

from ml_wrapper.validators.data_schema import Constants


class BaseDataset(Dataset):
    def __init__(self, data: Constants) -> None:
        self.dataframe = pandas.read_csv(data.data_file)

    def __len__(self):
        return self.dataframe.shape[0]

    def __getitem__(self, item):
        return self.dataframe.iloc[item]


class LendingClubDataset(Dataset):
    def __init__(self, X: Any, y: Any, X_transform: Any = None, y_transform: Any = None):

        """
        Args:
            X: Numpy ndarray
            y: Numpy ndarray
            X_transform: Any transform that works with Numpy arrays
            y_transform: Any transform that works with Numpy arrays
        """
        super().__init__()
        self.X = X
        self.Y = y
        self.X_transform = X_transform
        self.y_transform = y_transform

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        x = self.X[idx].type(torch.float32)

        if self.X_transform:
            x = self.X_transform(x)

        if self.Y is None:
            return x

        y = self.Y[idx]

        # Do not convert integer to float for classification validators
        if not y.dtype in [torch.int32, torch.int64]:
            y = y.type(torch.float32)

        if self.y_transform:
            y = self.y_transform(y)
        return x, y.type(torch.LongTensor)
