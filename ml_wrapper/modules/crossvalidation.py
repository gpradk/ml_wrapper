import pytorch_lightning as pl
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle as sk_shuffle
from torch.utils.data import DataLoader

from ml_wrapper.modules.dataset import LendingClubDataset
from ml_wrapper.validators.lightning_classification_schema import DataLoaderSchema


class CrossValidationDataModule(pl.LightningDataModule):
    """
    Cross-validation lightning datamodule.

    """

    def __init__(
        self,
        X,
        y,
        kf,
        hold_out_split=0.2,
        num_workers=2,
        random_state=1234,
        shuffle=True,
        batch_size: int = 16,
        pin_memory=False,
        drop_last=False,
        *args,
        **kwargs,
    ):

        super().__init__(*args, **kwargs)
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.pin_memory = pin_memory
        self.drop_last = drop_last
        self.hold_out_split = hold_out_split
        self.random_state = random_state
        # shuffle x and y
        if shuffle:
            X, y = sk_shuffle(X, y, random_state=random_state)
        else:
            raise ModuleNotFoundError(  # pragma: no-cover
                "You want to use shuffle function from `scikit-learn` which is not installed yet."
            )

        self.split_dataloader_list = []
        self.splits = None
        self.cv_splits = []
        self.get_cv_dataloaders(X, y, kf)

    def _init_datasets(self, X, y, x_val, y_val, x_holdout, y_holdout):
        self.train_dataset = LendingClubDataset(X, y)
        self.val_dataset = LendingClubDataset(x_val, y_val)
        self.test_dataset = LendingClubDataset(x_holdout, y_holdout)

    def train_dataloader(self):
        return DataLoader(**DataLoaderSchema(dataset=self.train_dataset).dict())

    def val_dataloader(self):
        return DataLoader(**DataLoaderSchema(dataset=self.val_dataset, shuffle=False).dict())

    def test_dataloader(self):
        return DataLoader(**DataLoaderSchema(dataset=self.test_dataset, shuffle=False).dict())

    def get_cv_dataloaders(self, x, y, kf):
        x_cv, x_holdout, y_cv, y_holdout = train_test_split(x, y, random_state=self.random_state, stratify=y)
        splits = kf.split(x_cv, y_cv)
        for cv_count, (train_index, test_index) in enumerate(splits):
            self.cv_splits.append({"train": train_index, "test": test_index})
            self._init_datasets(
                x_cv[train_index], y_cv[train_index], x_cv[test_index], y_cv[test_index], x_holdout, y_holdout
            )
            loader_dict = {
                "train_dataloader": self.train_dataloader(),
                "val_dataloader": self.val_dataloader(),
                "test_dataloader": self.test_dataloader(),
            }
            self.split_dataloader_list.append(loader_dict)

    def cv_dataloaders(self):
        return self.split_dataloader_list
