import torch
import torch.nn as nn
import torch.nn.functional as F


class MLP(nn.Module):
    """Simple MLP model"""

    def __init__(self, input_dim, num_classes, hidden_dim, n_hidden, bias, dropout):

        super().__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.num_classes = num_classes
        self.n_hidden = n_hidden
        self.dropout = dropout
        self.bias = bias
        self.linear_in = torch.nn.Linear(in_features=self.input_dim, out_features=self.hidden_dim, bias=self.bias)

        self.linear_layers = torch.nn.ModuleList(
            [torch.nn.Linear(self.hidden_dim, self.hidden_dim, bias=self.bias) for i in range(self.n_hidden)]
        )
        self.linear_out = torch.nn.Linear(in_features=self.hidden_dim, out_features=self.num_classes, bias=self.bias)

        self.linear_in.apply(self.init_weights)
        self.linear_out.apply(self.init_weights)
        for i, layer in enumerate(self.linear_layers):
            layer.apply(self.init_weights)

    def forward(self, x):
        x_in = self.linear_in(x)
        for i, layer in enumerate(self.linear_layers):
            x_in = layer(x_in)
            x_in = F.relu(x_in)
            x_in = nn.Dropout(p=self.dropout)(x_in)
        return F.relu(self.linear_out(x_in))

    @staticmethod
    def init_weights(m):
        if type(m) == torch.nn.Linear:
            torch.nn.init.xavier_uniform_(m.weight)
