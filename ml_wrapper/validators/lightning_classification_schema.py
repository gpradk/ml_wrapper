import os
from pathlib import Path
from typing import Optional, Any

from pydantic import BaseModel, validator, conint, confloat
from pydantic.typing import Literal

# Files
PKG_PATH = Path(__file__).parents[2].resolve()
MODELS_DIR = PKG_PATH / "ml_wrapper/saved_models/checkpoints"
MODEL_FILE = MODELS_DIR / "nn_model.ckpt"

# Job
CPUS = 4
GPUS = 1
RANDOM_STATE = 42
VERBOSE = 0

# Trainer
BATCH_SIZE = 128
HOLD_OUT_SPLIT = 0.2
LEARNING_RATE = 1e-4
MIN_EPOCHS = 1
MAX_EPOCHS = 10

# Callbacks
MONITOR = "val_loss"
MIN_DELTA = 1e-4
MODE = "min"
PATIENCE = 5
SHUFFLE = True

# Dataloader:
DROP_LAST = False
PIN_MEMORY = False

# CV:
N_SPLITS = 3
N_REPEATS = 1
N_TRIALS = 2

# Model:
L1_STRENGTH = 1e-6
L2_STRENGTH = 1e-4

# MLP:
DROPOUT = 0.5
HIDDEN_DIM = 128
N_HIDDEN = 2


class CheckPointSchema(BaseModel):
    filename: str = MODEL_FILE
    dirpath: Literal[MODELS_DIR] = MODELS_DIR
    monitor: Literal["val_loss"] = MONITOR
    save_top_k: Literal[conint(gt=0)] = 1

    @validator("dirpath")
    def check_dirpath(cls, value):
        if not os.path.exists(value):
            os.makedirs(value)


class EarlyStoppingSchema(BaseModel):
    monitor = MONITOR
    min_delta: Literal[conint(ge=0)] = MIN_DELTA
    patience: Literal[conint(ge=0)] = PATIENCE
    verbose: Literal[conint(ge=0)] = VERBOSE
    mode: Literal[MODE] = MODE


class NNJobParam(BaseModel):
    cpus: Literal[conint(gt=0)] = CPUS
    random_state: Literal[42] = RANDOM_STATE
    verbose: Literal[conint(ge=0)] = VERBOSE
    batch_size: Literal[conint(ge=1)] = BATCH_SIZE
    model_file: Literal[MODEL_FILE] = MODEL_FILE
    checkpoint_params = CheckPointSchema()
    early_stopping_params = EarlyStoppingSchema()
    shuffle: Literal[True] = SHUFFLE


class BaselineModelSchema(BaseModel):
    input_dim: int
    num_classes: int
    bias: bool = True
    learning_rate: Literal[confloat(gt=0, lt=1)] = LEARNING_RATE
    l1_strength: float = L1_STRENGTH
    l2_strength: float = L2_STRENGTH
    optimizer: Literal[f"Adam", "SGD"]


class MLPModelSchema(BaseModel):
    input_dim: int
    num_classes: int
    output_dim: int
    bias: bool = True
    hidden_dim: int = HIDDEN_DIM
    n_hidden: int = N_HIDDEN
    l1_strength: float = L1_STRENGTH
    l2_strength: float = L2_STRENGTH
    dropout: float = DROPOUT
    learning_rate: Literal[confloat(gt=0, lt=1)] = LEARNING_RATE
    optimizer: Literal[f"Adam", "SGD"]


class CVParam(BaseModel):
    n_splits: Literal[conint(ge=2)] = N_SPLITS
    n_repeats: Literal[conint(gt=0)] = N_REPEATS
    n_trials: Literal[conint(gt=0)] = N_TRIALS
    hold_out_split: Literal[confloat(gt=0.1, lt=0.9)] = HOLD_OUT_SPLIT
    shuffle: Literal[True] = SHUFFLE


class TrainerSchema(BaseModel):
    auto_lr_find: bool = True
    gpus: Optional[Literal[conint(gt=0)]] = GPUS
    callbacks: list = []
    max_epochs: Literal[conint(ge=MIN_EPOCHS)] = MAX_EPOCHS
    min_epochs: Literal[conint(gt=0)] = MIN_EPOCHS


class DataLoaderSchema(BaseModel):
    dataset: Any
    batch_size: Literal[conint(ge=1)] = BATCH_SIZE
    shuffle: Literal[True, False] = SHUFFLE
    num_workers: Literal[conint(gt=0)] = CPUS
    drop_last: Literal[True] = DROP_LAST
    pin_memory: Literal[True] = PIN_MEMORY
