""" Setup file for project."""

from setuptools import setup, find_packages


setup(
    name="ml_wrapper",
    version="0.0.1",
    description="ML model wrapper for a tabular dataset.",
    author="Pradeep Ganesan",
    packages=find_packages(),
    install_requires=[
        "pytorch_lightning",
        "pandas",
        "numpy",
        "scikit-learn",
        "optuna",
        "pydantic",
        "pandera",
        "pytest-runner",
        "pytest",
    ],
)
